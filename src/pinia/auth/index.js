// import VueCookies from "vue-cookies";
import { defineStore } from "pinia";
import { decodeCookie, getCookie, setCookie, clearCookie, userInfoKey } from '@/utils/cookie'

// if (env !== "local") {
//   VueCookies.config("1D", "/crm");
// }

const userInfo = getCookie(userInfoKey) || {};
export const actions = {
  SET_USER_INFO: "setUserInfo",
};

export default defineStore({
  id: "auth",
  state: () => {
    return {
      userInfo: userInfo
    }
  },
  actions: {
    setUserInfo(userInfo) {
      this.userInfo = decodeCookie(userInfo);
      if (userInfo) {
        setCookie(userInfo)
      } else {
        clearCookie()
      }
    },
  },
})