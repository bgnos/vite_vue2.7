import { defineStore } from "pinia";

export default defineStore({
  id: "menu",
  state: () => {
    return {
      menu: [
        {
          title: "项目列表",
          path: "/project/list",
          icon: "el-icon-s-order",
        },
        {
          title: "error",
          path: "/error",
          icon: "el-icon-warning-outline",
        },
        {
          title: "登录页",
          path: "/login",
          icon: "el-icon-user-solid",
        },
        {
          icon: "el-icon-menu",
          title: "父级菜单",
          submenus: [
            {
              title: "子菜单",
              path: "/",
            },
            {
              title: "子菜单",
              path: "/",
              submenus: [
                {
                  title: "子菜单",
                  path: "/",
                },
                {
                  title: "子菜单",
                  path: "/",
                },
                {
                  title: "子菜单",
                  path: "/",
                },
              ],
            },
            {
              title: "子菜单",
              path: "/",
            },
          ],
        },
        {
          icon: "el-icon-menu",
          title: "父级菜单",
          submenus: [
            {
              title: "子菜单",
              path: "/",
            },
            {
              title: "子菜单",
              path: "/",
              submenus: [
                {
                  title: "子菜单",
                  path: "/",
                },
                {
                  title: "子菜单",
                  path: "/",
                },
                {
                  title: "子菜单",
                  path: "/",
                },
              ],
            },
            {
              title: "子菜单",
              path: "/",
            },
          ],
        },
      ],
    };
  },
});
