import { Loading } from 'element-ui';

export default function (Vue) {
  Vue.use(Loading.directive);
}
