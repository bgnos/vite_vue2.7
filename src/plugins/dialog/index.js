import dialog from "./dialog";
import DialogWrap from "./component/dialog-wrap.vue";

let openDialog = null;

function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  openDialog = (comp, propsData) => dialog(Vue, comp, propsData);

  Vue.prototype.$openDialog = openDialog;

  Vue.component("dialog-wrap", DialogWrap);

  Vue.mixin({
    methods: {
      $closeDialog(...arg) {
        this.$emit("close", ...arg);
      },
    },
  });
}

// auto plugin install
let GlobalVue = null;
if (typeof window !== "undefined") {
  GlobalVue = window.Vue;
} else if (typeof global !== "undefined") {
  GlobalVue = global.vue;
}
if (GlobalVue) {
  GlobalVue.use({
    install,
  });
}

// export default
export default {
  install,
};

export { openDialog };
