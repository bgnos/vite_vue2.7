import { PopupManager } from "element-ui/lib/utils/popup";

export default (Vue, component, propsData = {}) => {
  const app = document.querySelector("#app-c");

  const ComponentConstructor = Vue.extend(component);
  let instance = new ComponentConstructor({
    propsData,
    parent: app.__vue__,
  }).$mount();

  app.appendChild(instance.$el);

  instance.$el.style.zIndex = PopupManager.nextZIndex();

  const destroyDialog = () => {
    if (instance) {
      instance.$destroy();
      app.removeChild(instance.$el);
      instance = null;
    }
  };

  return new Promise((resolve) => {
    instance.$once("close", (...arg) => {
      destroyDialog();
      resolve(...arg);
    });
  });
};
