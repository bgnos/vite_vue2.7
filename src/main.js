// import "uno.css";
import Vue from "vue";
import App from "./App.vue";

// import * as Sentry from "@sentry/vue";
// import { BrowserTracing } from "@sentry/tracing";

import router from "./router";
import { createPinia, PiniaVuePlugin } from "pinia";

import components from "./components";
import plugins from "./plugins";
// import directives from "./directives";

import "element-ui/lib/theme-chalk/index.css";
import "./style/index.less";

const pinia = createPinia();

Vue.use(PiniaVuePlugin);
Vue.use(pinia);

Vue.use(components);
Vue.use(plugins);
// Vue.use(directives);

Vue.config.productionTip = false;
window.addEventListener(
  "unhandledrejection",
  function browserRejectionHandler(event) {
    event && event.preventDefault();
  }
);

if (import.meta.env === "production") {
  Sentry.init({
    Vue,
    dsn: "https://2b40908221ab4f01a9e0ff8e9972bbe9@femonitor.51job.com/10",
    integrations: [
      new BrowserTracing({
        routingInstrumentation: Sentry.vueRouterInstrumentation(router),
        tracingOrigins: ["localhost", "my-site-url.com", /^\//],
      }),
    ],
    tracesSampleRate: 1.0,
  });
}

new Vue({
  router,
  pinia,
  render: (h) => h(App),
}).$mount("#app");
