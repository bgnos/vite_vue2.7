import { getCookie, userInfoKey } from '@/utils/cookie'

export default function (to, from, next) {
  const userInfo = getCookie(userInfoKey) || {};
  if (!userInfo?.authtoken && to.path !== '/login') {
    next({ path: '/login' })
  } else {
    next()
  }
}