export default [
  {
    path: '/project',
    name: "Project",
    component: () => import('@/components/layout/Layout.vue'),
    children: [
      {
        path: 'list',
        name: "ProjectList",
        component: () => import('@/views/Project'),
      }
    ]
  }
]