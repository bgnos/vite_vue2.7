import BaseRoutes from './base'
import ProjectRoutes from './project'

export default [
  ...ProjectRoutes,
  ...BaseRoutes,// BaseRoutes 匹配优先级应为最低，其他路由加在 BaseRoutes 上面
]