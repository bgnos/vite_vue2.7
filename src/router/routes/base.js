export default [
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/Login"),
  },
  {
    path: "/template",
    name: "Template",
    component: () => import("@/Template"),
  },
  {
    path: "/",
    redirect: "/welcome",
  },
  {
    path: "",
    component: () => import("@/components/layout/Layout.vue"),
    children: [
      {
        path: "/welcome",
        name: "welcome",
        meta: { title: "项目订单" },
        component: () => import("@/views/Base/Welcome.vue"),
      },
    ],
  },
  {
    path: "/403",
    name: "403",
    meta: { title: "无权访问", menu: "403" },
    component: () => import("@/views/Base/Error.vue"),
  },
  {
    path: "/404",
    name: "404",
    meta: { title: "页面不存在", menu: "404" },
    component: () => import("@/views/Base/Error.vue"),
  },
  {
    path: "/error",
    name: "error",
    meta: { title: "参数错误", menu: "error" },
    component: () => import("@/views/Base/Error.vue"),
  },
  {
    path: "*",
    redirect: "/404",
  },
];
