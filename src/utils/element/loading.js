// 基于 element-ui 的 Loading 封装，element-ui 的 Loading.service() 返回的都是同一个 Loading 实例，且其中任何一个调用 close 方法都会导致 Loading 直接关闭
// 通过 show、close 和 counter 来管理这个单一 loading 实例
import { Loading } from "element-ui";

let loadingInstance = null;
let count = 0;

function show() {
  if (count === 0) {
    loadingInstance = Loading.service({
      lock: true,
      text: "Loading",
    });
  }
  count++;
}

function close() {
  count--;
  if (count === 0) {
    loadingInstance.close();
  }
}

export default { show, close };
