
import { Message } from 'element-ui';

const {
  success,
  warning,
  error,
  info
} = Message

export default {
  ...Message,
  success: (...args) => {
    Message.closeAll()
    return success(...args)
  },
  warning: (...args) => {
    Message.closeAll()
    return warning(...args)
  },
  error: (...args) => {
    Message.closeAll()
    return error(...args)
  },
  info: (...args) => {
    Message.closeAll()
    return info(...args)
  },
}