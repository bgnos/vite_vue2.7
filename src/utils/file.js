import downloadjs from "downloadjs";
import { Loading } from "./element";

export const toDownload = async (record) => {
  Loading.show();
  let fileInfo = record;
  fileInfo.filepath = record.filepath || record.url;
  fileInfo.filename = record.filename || record.name;

  try {
    const result = await downloadFile({
      filepath: record.filepath,
      token: record.token,
    });
    downloadAction(fileInfo, result);
  } catch {
    Modal.warning({
      title: "提示",
      content: "下载出错！",
    });
  }
  Loading.remove();
};
export const downloadAction = async (record, result) => {
  try {
    const filePath = record.filepath;
    const ext = filePath.substring(filePath.lastIndexOf(".") + 1).toLowerCase();
    const fileName = record.filename.replaceAll("." + ext, "") + "." + ext;
    if (ext == "pdf" || ext == "png" || ext == "jpg" || ext == "jpeg") {
      let type = "";
      if (ext == "pdf") {
        type = "application/pdf";
      } else type = "image/" + ext;
      const binaryData = [];
      binaryData.push(result);
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(
          new Blob(binaryData, { type: type }),
          fileName
        );
      } else {
        window.open(
          window.URL.createObjectURL(new Blob(binaryData, { type: type }))
        );
      }
    } else {
      downloadjs(result, fileName);
    }
  } catch {
    Modal.warning({
      title: "提示",
      content: "下载出错！",
    });
  }
};
