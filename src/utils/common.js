import { Message, Loading } from "./element";

export const fetchData = async (api, params, isLoading = true) => {
  if (isLoading) Loading.show();
  return api(params)
    .then((result) => {
      if (result.code == "0") {
        Loading.close();
        return Promise.resolve(result.data);
      } else {
        return Promise.reject(result);
      }
    })
    .catch((err) => {
      Loading.close();
      Message.error(err.message);
      throw err.message;
    });
};
