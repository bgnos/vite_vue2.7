import VueCookies from "vue-cookies";

export const userInfoKey = "CRMPLUS";
export const JSESSIONIDKey = "JSESSIONID";

//解析cookie
export function decodeCookie(value) {
  let userInfo = {};
  const decodeValueArr = decodeURIComponent(value).split("&");
  decodeValueArr.forEach((item) => {
    const splitItem = item.split("=");
    if (splitItem[0] == "cname") {
      splitItem[1] = decodeURI(splitItem[1]);
    }
    userInfo[splitItem[0]] = splitItem[1];
  });
  return userInfo;
}

//从浏览器中获取 cookie
export function getCookie(name) {
  var prefix = name + "=";
  var start = document.cookie.indexOf(prefix);
  if (start == -1) {
    return null;
  }
  var end = document.cookie.indexOf(";", start + prefix.length);
  if (end == -1) {
    end = document.cookie.length;
  }
  var value = document.cookie.substring(start + prefix.length, end);
  let userInfo = {};
  if (value && value != "") {
    userInfo = decodeCookie(value);
    VueCookies.set(JSESSIONIDKey, userInfo.sessionid);
  }
  return userInfo || {};
}

export function setCookie(userInfo) {
  VueCookies.set(userInfoKey, userInfo);
}

export function clearCookie() {
  VueCookies.set(userInfoKey, "");
}