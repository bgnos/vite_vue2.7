// 集中管理需要全局注册的组件

import SubMenu from "./layout/SubMenu";
import MenuItem from "./layout/MenuItem";
export default function (Vue) {
  Vue.component("sub-menu", SubMenu);
  Vue.component("menu-item", MenuItem);
}
