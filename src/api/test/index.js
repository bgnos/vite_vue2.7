import { post } from "@/http";

import serviceGenerator from "@/http/utils/serviceGenerator.js";
import httpTemplate from "@/http/utils/indexTemplate.js";

const serviceDefaultConfig = {
  baseURL: import.meta.env.VITE_TEST_URL, // url = base url + request url
  timeout: 60000, // request timeout
  withCredentials: true,
  headers: {
    "Content-Type": "application/json",
    "Cache-Control": "no-cache",
    Pragma: "no-cache",
  },
};

const service = serviceGenerator(serviceDefaultConfig);
const { upload } = httpTemplate(service, "sales/");

export const test = (params) => post("/account/getsaleslist", params);

export const uploadTest = (params) =>
  upload("/account/uploadapproveletter", params);
