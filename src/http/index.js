import { service } from "./utils/serviceGenerator";
import httpTemplate from "./utils/indexTemplate";

const { post, get, upload, blobPost, blobGet } = httpTemplate(service, "sales/");

export { post, get, upload, blobPost, blobGet };
export default { post, get, upload, blobPost, blobGet };
