
import trim from "lodash/trim";

// 合并 URL 项
// 自动去除多余的 /
const combineUrl = (...urls) => urls.map((url) => trim(url, "/")).join("/");

/**
 * serviceUrl 微服务URL前缀
 */
export default (service, serviceUrl) => ({
  /**
   * post方法，对应post请求
   * @param {String} url [请求的url地址]
   * @param {Object} params [请求时携带的参数]
   */
  post: (url, params) => {
    return service.post(combineUrl(serviceUrl, url), params);
  },

  /**
   * get方法，对应get请求
   * @param {String} url [请求的url地址]
   * @param {Object} params [请求时携带的参数]
   */
  get: (url, params) => {
    return service.get(combineUrl(serviceUrl, url), { params });
  },

  /**
   * 上传文件方法
   * @param {String} url 请求的URL地址
   * @param {Form} data 请求的multipart/form-data
   * @returns 请求 Promise
   */
  upload: (url, data) =>
    service.request({
      params: { crm_service_http_type: "upload" },
      url: combineUrl(serviceUrl, url),
      data: data,
    }),

  blobPost: (url, params) =>
    service.post(combineUrl(serviceUrl, url), params, { responseType: "blob" }),

  blobGet: (url, params) =>
    service.get(
      combineUrl(serviceUrl, url),
      { params },
      { responseType: "blob" }
    ),
});
