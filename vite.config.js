import { splitVendorChunkPlugin } from "vite";
import vue from "@vitejs/plugin-vue2";
import legacy from "@vitejs/plugin-legacy";
import Components from "unplugin-vue-components/vite";
import { ElementUiResolver } from "unplugin-vue-components/resolvers";
import path from "path";

export default () => {
  return {
    server: {
      host: true,
      port: 28847,
    },
    plugins: [
      vue(),
      splitVendorChunkPlugin(),
      legacy({
        targets: ["ie 11", "ie 10"],
        polyfills: true,
        modernPolyfills: true,
        additionalLegacyPolyfills: [
          "core-js/stable",
          "regenerator-runtime/runtime",
        ],
      }),
      Components({
        resolvers: [ElementUiResolver()],
      }),
    ],
    build: {
      target: "es2015",
      chunkSizeWarningLimit: 2000,
      rollupOptions: {
        output: {
          manualChunks: {
            "element-ui": ["element-ui"],
          },
        },
      },
    },
    resolve: {
      alias: {
        "@": path.join(__dirname, "./src"),
      },
      extensions: [".vue", ".mjs", ".js", ".ts", ".jsx", ".tsx", ".json"],
    },
  };
};
